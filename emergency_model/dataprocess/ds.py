import numpy as np
import pandas as pd;

def makeds(filepath,outpath,columns='R',clasify="Incident Type",type="Fire-"):
    
    df=pd.read_csv(filepath,delimiter=",",index_col=None)
    df = df[df[clasify].str.contains(type)==True]
    df = df[df[clasify].str.contains("Alarm")==False]
    print df["Incident Type"]
    df["Response Time"] = (pd.to_datetime(df["Closed Date"]) - pd.to_datetime(df["Creation Date"])).dt.total_seconds()
    df["minute"]=60
    df["Response Time"]=df["Response Time"].div(df["minute"])
    df["Response Time"]=df["Response Time"].astype(int)
    df["Creation Date"] = pd.to_datetime(df["Creation Date"])
    df["Creation Date"] = df["Creation Date"].dt.date
    listCols = {'C': "Creation Date", 'R': "Response Time", 'B': "Borough", 'L': "Location", 'T': "Incident Type",
                'E': "Closed Date"}
    listCols = [listCols[column] for column in columns]
    df=df[listCols]
    df.to_csv(outpath,index=False)

makeds("~/Desktop/rt/t1.csv","~/Desktop/rt/FO.csv",columns="TCR",clasify='Incident Type',type="Fire-")
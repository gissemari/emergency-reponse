# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Just change a little code in lstm model.

 Set the PYTHONPATH variable with the current directory:
	- export PYTHONPATH=$PYTHONPATH:/home/likle/Desktop/emergency
- The parameters that you can choose to run LSTM are:
	- fileName: location for input file
	- input-dim: number of variables at each time step
	- inputCols: R (Response Time)
	- path: path for the output location
	- x-length
	- y-length
	- minEpoch: epochs
	- learningRate
	- hiddenLayer: number of hidden units
	- numLayers: number of stacked layers
- You can run LSTM:
	python model/seq2seq_unguided_LSTM.py --fileName Data/FO.csv --input-dim 1 --inputCols R --path Results/lstm --x-length 10 --y-length 5 --minEpoch 500 --learningRate 0.01 --hiddenLayer 10 --numLayers 1